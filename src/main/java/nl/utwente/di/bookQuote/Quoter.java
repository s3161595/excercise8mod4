package main.java.nl.utwente.di.bookQuote;

public class Quoter {

    double getBookPrice(String celsius){
        return celsius*1.8 + 32;
    }

}
